#!/bin/bash

#make a user directory
mkdir -p /users

if [[ ! -f /users/users.txt ]]; then
	touch /users/users.txt
fi

flag2=0

while [ $flag2 -eq 0 ];
do
#prompting the user to input their name
read -p "Enter Username: " name
	#Checking if the username already exist
	if awk -v name="$name" -F ',' '$1 == name {found=1} END {exit !found}' /users/users.txt; then 
		echo "Username already exists, please pick another one"
		echo "$(date +'%y/%m/%d %H:%M:%S') REGISTER: ERROR User already exists" >> log.txt
	else flag2=1
	fi
done
#initiate a flag variable to check the password requirements
flag=0

#while loop to read the password and check if it meets the requirements
while [ $flag -eq 0 ];
do
	#prompting the user to input their password
	read -p "Enter Password: " password
	echo
	#checking if the password contains at least 8 characters
	if [ ${#password} -ge 8 ]; then
		#checking if the password contains at least 1 uppercase and 1 lowercase
		if [[ "$password" =~ [A-Z] ]] && [[ "$password" =~ [a-z] ]]; then
			#checking if the password contains numbers
			if [[ "$password" =~ [0-9] ]]; then
				#checking if the password is the same as username
				if [[ ! "$password" =~ "$name" ]]; then
					#checking if the password contains forbidden words such as chicken or ernie
					if  echo  "$password" | awk '/chicken|ernie/ {exit 1}'; then
					flag=1
					else
					echo " Password shall not contain chicken nor ernie"
					fi
				else
				echo "Password cannot be the same as username"
				fi
			else
				echo "Password must be alphanumeric (contains atleast 1 number)"
			fi
		else
			echo "Password must contain at least 1 uppercase and 1 lowercase"
		fi
	elif [ ${#password} -lt 8 ]; then
		echo "Password must be 8 characters long"
	fi
	done

#make and append username and password to txt file
echo "$name,$password" >> /users/users.txt
echo "$(date +'%y/%m/%d %H:%M:%S') REGISTER: INFO user $name registered succesfully" >> log.txt
echo "REGISTER DONE"
