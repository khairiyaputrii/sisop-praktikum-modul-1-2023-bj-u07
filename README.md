# sisop-praktikum-modul-1-2023-BJ-U07



## Modul 1 Sisop 2023 U07 Formal Report

Group Members:
1. Khairiya Maisa Putri (5025211192)
2. Fauzan Ahmad Faisal (5025211067)
3. Muhammad Fadhlan Ashila Harashta (5025211068)

# No 1

## Question

>>Bocchi hendak melakukan University Admission Test di Jepang. Bocchi ingin masuk ke universitas yang bagus. Akan tetapi, dia masih bingung sehingga ia memerlukan beberapa strategi untuk melakukan hal tersebut. Untung saja Bocchi menemukan file .csv yang berisi ranking universitas dunia untuk melakukan penataan strategi  : 
>>-  Bocchi ingin masuk ke universitas yang bagus di Jepang. Oleh karena itu, Bocchi perlu melakukan survey terlebih dahulu. Tampilkan 5 Universitas dengan ranking tertinggi di Jepang.
>>-  Karena Bocchi kurang percaya diri, coba cari Faculty Student Score(fsr score) yang paling rendah diantara 5 Universitas dari hasil filter poin a.
>>-  Karena Bocchi takut salah membaca ketika memasukkan nama universitas, cari 10 Universitas di Jepang dengan Employment Outcome Rank(ger rank) paling tinggi.
>>-  Bocchi ngefans berat dengan universitas paling keren di dunia. Bantu bocchi mencari universitas tersebut dengan kata kunci keren.

Source code for No. 1:
```shell script
#!/bin/bash

echo -e "Here are the top 5 of the University in Japan based on the 2023 QS World University Rankings:\n"

awk '/Japan/{print}' '2023 QS World University Rankings.csv' | head -n 5

echo -e "\nThe university with the lowest Faculty Student Score in Japan is:\n"

grep -E Japan '2023 QS World University Rankings.csv' | sort -t, -k9n | head -n 5

echo -e "\nHere are the top 10 university in Japan that has the highest Employment Outcome Rank:\n"

awk '/Japan/' '2023 QS World University Rankings.csv' | sort -t, -k20n | head -n 10


answer="keren"
another_answer="Keren"
echo -e "\nplease enter the keyword:"
read keyword

if [ $keyword  == $answer ]
then 
awk '/Keren/{print}' '2023 QS World University Rankings.csv'
elif [ $keyword == $another_answer ]
then
awk '/Keren/{print}' '2023 QS World University Rankings.csv'
else
echo "sorry wrong keyword"
fi

```
# Explanation

**First** we make a file called ```university_survey.sh``` where we store all the code for No 1

```shell script
$ nano universiry_survey.sh
```

## A
For this question, we were asked to make a program that will show 5 University in Japan with the highest ranks.

```shell script
#!/bin/bash

echo -e "Here are the top 5 of the University in Japan based on the 2023 QS World University Rankings:\n"

awk '/Japan/{print}' '2023 QS World University Rankings.csv' | head -n 5

```
First, we use ```awk '/Japan/{print}' '2023 QS World University Rankings.csv'``` to find all the datas from the 2023 QS World University Ranking.csv file that included the word "Japan" on them, and then use ```head -n 5``` to get the first 5 datas on the list with numerical order

## B
For this question, we were asked to make a program that will show the university with the lowest Faculty Student Score (fsr score) in Japan.

```shell script
echo -e "\nThe university with the lowest Faculty Student Score in Japan is:\n"

grep -E Japan '2023 QS World University Rankings.csv' | sort -t, -k9n | head -n 5

```
First, we use ```grep -E Japan``` to search all datas that contain the word "Japan", from the .csv file, and then use ```sort -t -k9n``` to sort the datas according to column 9, by numerical order. Because we are looking for the 1 university with the lowest fsr score, we use ```head -n ``` to get the data that is on the top 5 from the sorting. 

## C
For this question, we were asked to make a program that will show 10 University in Japan with the highest Employment Outcome Rank (ger rank).

```shell script
echo -e "\nHere are the top 10 university in Japan that has the highest Employment Outcome Rank:\n"

awk '/Japan/' '2023 QS World University Rankings.csv' | sort -t, -k20n | grep -m 10 Japan

```
First, we use ```awk '/Japan/'``` to find all the data that contain the word "Japan" from the .csv file. After that, we use ```sort -t, -k20n``` to sort the datas according to column 20, by numerical order. Because we are looking for the 10 university with the highest ger rank, we use ```head -n 10``` to get the data that is on the top 10 from the sorting.

## D
For this question, we were asked to make a program that will show the most cool university in the world when we input the word "keren".

```shell script
answer="keren"
another_answer="Keren"
echo -e "\nplease enter the keyword:"
read keyword

if [ $keyword  == $answer ]
then 
awk '/Keren/{print}' '2023 QS World University Rankings.csv'
elif [ $keyword == $another_answer ]
then
awk '/Keren/{print}' '2023 QS World University Rankings.csv'
else
echo "sorry wrong keyword"
fi

```
1. I declared 3 string variables which are ```keyword``` , ```answer``` and ```another_answer```, and the string value of variable ```answer``` and ```another_answer``` are ```keren``` and ```Keren```` , while the ```keyword``` variable will be the variable for the input. I declared 2 of those so that we can input the word keren with both lowercase or uppercase.
2. ```If Else``` to compare the input from the user to the real keyword. 
3. if the conditions is true, then the program will make an output of the University that has the word "Keren" in it. To do that, we use ```awk '/Keren/{print}' '2023 QS World University Rankings.csv'``` to find the university with the word "Keren" from the .csv file.
4. if the conditions are false, then the program will make an output of "sorry wrong keyword"

# No 2

## Question

Kobeni ingin pergi ke negara terbaik di dunia bernama Indonesia. Akan tetapi karena uang Kobeni habis untuk beli headphone ATH-R70x, Kobeni tidak dapat melakukan hal tersebut. 
1. Untuk melakukan coping, Kobeni mencoba menghibur diri sendiri dengan mendownload gambar tentang Indonesia. Coba buat script untuk mendownload gambar sebanyak X kali dengan X sebagai jam sekarang (ex: pukul 16:09 maka X nya adalah 16 dst. Apabila pukul 00:00 cukup download 1 gambar saja). Gambarnya didownload setiap 10 jam sekali mulai dari saat script dijalankan. Adapun ketentuan file dan folder yang akan dibuat adalah sebagai berikut:

- File yang didownload memilki format nama “perjalanan_NOMOR.FILE” Untuk NOMOR.FILE, adalah urutan file yang download (perjalanan_1, perjalanan_2, dst)

- File batch yang didownload akan dimasukkan ke dalam folder dengan format nama “kumpulan_NOMOR.FOLDER” dengan NOMOR.FOLDER adalah urutan folder saat dibuat (kumpulan_1, kumpulan_2, dst) 
2. Karena Kobeni uangnya habis untuk reparasi mobil, ia harus berhemat tempat penyimpanan di komputernya. Kobeni harus melakukan zip setiap 1 hari dengan nama zip “devil_NOMOR ZIP” dengan NOMOR.ZIP adalah urutan folder saat dibuat (devil_1, devil_2, dst). Yang di ZIP hanyalah folder kumpulan dari soal di atas.

Source code for no. 2
```shell script
#!/bin/bash
image="https://upload.wikimedia.org/wikipedia/commons/thumb/e/ed/Bung_Tomo.jpg/640px-Bung_Tomo.jpg"


download(){
    if [[ ! -f counter ]] ; then
        echo 1 > counter
    fi
   
    if [[ -f counter ]] ; then
        counter=$(cat counter)
    fi
   
    if [[ ! -d "kumpulan_$counter" ]] ; then
        mkdir -p kumpulan_$counter
        counter=$((counter+1))
        echo $counter > counter
    fi
   
    count=$(cat counter)
    hour=$(date +'%H')
    hour=$((10#$hour))
    if [[ $hour -eq 0 ]] ; then
        wget "$image" -o "perjalanan_1.jpg"
    else
        for i in $(seq 1 $hour) ; do
            wget "$image" -O "perjalanan_$i.jpg"
        done
    fi
   
    mv perjalanan_*.jpg "kumpulan_$((counter-1))"
}


zipfile(){


    if [[ ! -f counter ]] ; then
        echo 1 > counter
    fi
   
    if [[ -f counter ]] ; then
        counter=$(cat counter)
    fi
   
    if [[ ! -d "kumpulan_$counter" ]] ; then
        counter=$((counter+1))
        echo $counter > counter
    fi
    count=$(cat counter)
   
    zip "devil_$((counter-1)).zip" kumpulan_*
}

case $1 in
    -download) download ;;
    -zip) zipfile ;;
esac

#Cronjob
#0 */12 * * * cd /home/fadhlan/sisop-praktikum-modul-1-2023-bj-u07/soal2 && ./kobeni_liburan.sh -download
#0 0 * * * cd /home/fadhlan/sisop-praktikum-modul-1-2023-bj-u07/soal2 && ./kobeni_liburan.sh -zip
```

Explanation
1.  The first thing to do is make a switch case for downloading the file and zipping the file. The case will call a function which will start each operation
    that will be done. We will use crontab for this operation.
2.  Next for download we check if counter file already exist, if it is not than we make a new file and insert one into the counter, after we done that we check if the counter file already exist, if it does read the counter and put it into a variable. Next we check if the file with the same number with the counter already exist if it doesn't we will make a directory with the name the current counter, and 1 will be added to the counter.
3.  Next is to download the file. As we want to download the file every 10 hour we first declare the hour. To avoid the time to be called 0X:XX we use the command as follows. Next is to check if the hour equal to 0, if yes then
    download the image once with the file name consist with number 1. Otherwise the program will continue to download the file until the program download it the same times as the current time is.
5.  Next, the file will move all the file that is downloaded to the directory that we created earlier.
6.  As for zipping fires we done the same thing to create counter.
7.  Next we check if the check if the zip file with tge same counter number already exist, if yes then the counter will be changed to counter+1.
8.  Lastly we zip all the file that is called kumpulan using kumpulan_*
9.  Next in crontab we set it to 10 hours for download and 1 day to zip. The cronjob      
    will   call both download and zip in respect of the timing.




# No 3

## Question
>>Peter Griffin hendak membuat suatu sistem register pada script **louis.sh** dari setiap user yang berhasil didaftarkan di dalam file /users/users.txt. Peter Griffin juga membuat sistem login yang dibuat di script **retep.sh**
>> 1. Untuk memastikan password pada register dan login aman, maka ketika proses input passwordnya harus memiliki ketentuan berikut
>>- Minimal 8 karakter
>>- Memiliki minimal 1 huruf kapital dan 1 huruf kecil
>>- Alphanumeric
>>- Tidak boleh sama dengan username 
>>- Tidak boleh menggunakan kata chicken atau ernie
>>2. Setiap percobaan login dan register akan tercatat pada log.txt dengan format : YY/MM/DD hh:mm:ss MESSAGE. Message pada log akan berbeda tergantung aksi yang dilakukan user.
>>- Ketika mencoba register dengan username yang sudah terdaftar, maka message pada log adalah REGISTER: ERROR User already exists
>>- Ketika percobaan register berhasil, maka message pada log adalah REGISTER: INFO User USERNAME registered successfully
>>- Ketika user mencoba login namun passwordnya salah, maka message pada log adalah LOGIN: ERROR Failed login attempt on user USERNAME
>>- Ketika user berhasil login, maka message pada log adalah LOGIN: INFO User USERNAME logged in

# Explanation
**First of all**, we make the shell script file by using the command
```shell script
$ nano louis.sh
```
this is a command to make the **louis** script used for the registration system

Next, we will start by making the /users directory to store the /users/users.txt file

```shell script
#!/bin/bash


#make a user directory
mkdir -p /users


if [[ ! -f /users/users.txt ]]; then
    touch /users/users.txt
fi
```
Here, we make a directory to store the users.txt used to store the names and passwords using ```mkdir -p /users```, then, we use the if statement to check whether or not the users.txt already exists within the directory, if not, we use the ```touch``` command to create the txt file


Next, we will prompt the user to input their desired username


```shell script
flag2=0


while [ $flag2 -eq 0 ];
do
#prompting the user to input their name
read -p "Enter Username: " name
    #Checking if the username already exist
    if awk -v name="$name" -F ',' '$1 == name {found=1} END {exit !found}' /users/users.txt; then
        echo "Username already exists, please pick another one"
        echo "$(date +'%y/%m/%d %H:%M:%S') REGISTER: ERROR User already exists" >> log.txt
    else flag2=1
    fi
done
```


Here, we used a while loop and using the ```flag2``` as a parameter to end the loop, then, ```read -p "Enter Username: " name``` will make it possible for users to input their username.


Then we use the condition ```if awk -v name="$name" -F ',' '$1 == name {found=1} END {exit !found}' /users/users.txt;``` to check whether or not the username they input already exists inside /users/users.txt, if it does, put an error message “Username already exists, please pick another one” and then it will also write the timestamp and the error message which is “REGISTER: ERROR User already exists” and then store it inside log.txt file. However, if the username doesn’t exist in /users/users.txt beforehand, the flag2 variable will then be set to 1 and it will exit the loop.


Next, we will prompt the user to input their passwords using another loop

```shell script
flag=0


#while loop to read the password and check if it meets the requirements
while [ $flag -eq 0 ];
do
    #prompting the user to input their password
    read -p "Enter Password: " password
    echo
    #checking if the password contains at least 8 characters
    if [ ${#password} -ge 8 ]; then
        #checking if the password contains at least 1 uppercase and 1 lowercase
        if [[ "$password" =~ [A-Z] ]] && [[ "$password" =~ [a-z] ]]; then
            #checking if the password contains numbers
            if [[ "$password" =~ [0-9] ]]; then
                #checking if the password is the same as username
                if [[ ! "$password" =~ "$name" ]]; then
                    #checking if the password contains forbidden words such as chicken or ernie
                    if  echo  "$password" | awk '/chicken|ernie/ {exit 1}'; then
                    flag=1
                    else
                    echo " Password shall not contain chicken nor ernie"
                    fi
                else
                echo "Password cannot be the same as username"
                fi
            else
                echo "Password must be alphanumeric (contains atleast 1 number)"
            fi
        else
            echo "Password must contain at least 1 uppercase and 1 lowercase"
        fi
    elif [ ${#password} -lt 8 ]; then
        echo "Password must be 8 characters long"
    fi
    done
```


This is a pretty similar while loop from the one we made before, the condition is also a flag variable, and all the if statements used here is to check whether or not the password that the user inputted has met all the requirements. The ```if [ ${#password} -ge 8 ];``` is used to check if the password has at least 8 characters, the ``` if [[ "$password" =~ [A-Z] ]] && [[ "$password" =~ [a-z] ]]; ``` is used to check if the password has at least one uppercase and one lowercase letters, the ``` if [[ "$password" =~ [0-9] ]]; ``` is to check if the password has at least 1 number, and lastly, the  ```  if  echo  "$password" | awk '/chicken|ernie/ {exit 1}'; ``` is used to check if the password contains forbidden words like “chicken” or “ernie”. If all the requirements are met, then set the flag variable to 1 and exit the loop. If not, just display a message on what they did wrong or what password requirement they didn’t meet. 


And lastly, we need to store the username and password into /users/users.txt, for that we use the following code

```shell script
echo "$name,$password" >> /users/users.txt
echo "$(date +'%y/%m/%d %H:%M:%S') REGISTER: INFO user $name registered succesfully" >> log.txt
echo "REGISTER DONE"
```


Here, we used ```echo``` to write the name and password variable to /users/users.txt and then write the timestamp and message “REGISTER: INFO user $name registered succesfully” to log.txt and then display the message “REGISTER DONE” to inform that users are properly registered.

Now, we can move to making the retep.sh for the login system


Make the file by using the command
```shell script
$ nano retep.sh
```
This is a rather simpler code compared to louis.sh, all we need is a loop to input the username and password, and then check if the username and password match the ones that exist inside the /users/users.txt


```shell script
#!/bin/bash


flag=0


while [ $flag -eq 0 ]; do
read -p "Enter Username: " name
    if awk -v name="$name" -F ',' '$1 == name {found=1} END {exit !found}' /users/users.txt; then
        read -p "Enter Password: " password
        if awk -v name="$name" -v password="$password" -F ',' '$2 == password && $1 == name {found=1} END {exit !found}' /users/users.txt; then
        flag=1
        echo "$(date +'%y/%m/%d %H:%M:%S') LOGIN: INFO User $name logged in" >> log.txt


        else
        echo "Password is Incorrect"
        echo "$(date +'%y/%m/%d %H:%M:%S') LOGIN: ERROR failed login attempt on user $name" >> log.txt


        fi
    else
        echo "Username does not exist"
    fi
done
echo "You are logged in succesfully"


flag2=0
```
We use ```read``` to prompt the user to input their username and password, then use the ``` if awk -v name="$name" -F ',' '$1 == name {found=1} END {exit !found}' /users/users.txt; ``` to check if their username exists inside the /users/users.txt, then we use ``` if awk -v name="$name" -v password="$password" -F ',' '$2 == password && $1 == name {found=1} END {exit !found}' /users/users.txt; ``` to check if the password is correct. If both conditions are met, exit the loop and print the timestamp and message “LOGIN: INFO User $name logged in" to log.txt file. If the conditions are not met, echo the message “Password is incorrect” and write the timestamp and the message “LOGIN: ERROR failed login attempt on user $name" to the log file.

# No 4

## Question
>>Johan Liebert adalah orang yang sangat kalkulatif. Oleh karena itu ia mengharuskan dirinya untuk mencatat log system komputernya. File syslog tersebut harus memiliki ketentuan : 
>>1. Backup file log system dengan format jam:menit tanggal:bulan:tahun (dalam format .txt).
>>2. Isi file harus dienkripsi dengan string manipulation yang disesuaikan dengan jam dilakukannya backup seperti berikut:
>>- Menggunakan sistem cipher dengan contoh seperti berikut. Huruf b adalah alfabet kedua, sedangkan saat ini waktu menunjukkan pukul 12, sehingga huruf b diganti dengan huruf alfabet yang memiliki urutan ke 12+2 = 14
>>- Hasilnya huruf b menjadi huruf n karena huruf n adalah huruf ke empat belas, dan seterusnya.
>>- Setelah huruf z akan kembali ke huruf a
>>3. Buat juga script untuk dekripsinya.
>>4. Backup file syslog setiap 2 jam untuk dikumpulkan 😀.

# Explanation
For this question we were asked to make a backup file where we have to encrypt the file contents by using a manipulator string. We use a cipher system to encrypt the contents. And we also have to make the decrypt file. And the script have to automatically do the backup file every 2 hours.

**First** we make a file called ```log_encrypt.sh``` where we will store the code that will do the encryption

```shell script
$ nano log_encrypt.sh
```
Source code for log_encrypt.sh:

```shell script
#!/bin/bash

hour=$(date "+%H")

date=$(date "+%H:%M %d:%m:%Y")

backup="/home/Documents/soal4/encrypt/${date}.txt"

lowercase="abcdefghijklmnopqrstuvwxyz"
uppercase="ABCDEFGHIJKLMNOPQRSTUVWXYZ"

shift_hour=$((hour % 26))

cat /var/log/syslog | tr "${lowercase}${uppercase}" "${lowercase:${shift_hour}}${uppercase:${shift_hour}}" > "${backup}"
```
1. ```hour=$(date "+%H")``` is to make the current time into a 24-hours format
2. We used ```date=$(date "+%H:%M %d:%m:%Y")``` to make the the current time and date in the format of HH:MM dd:mm:YY
3. on the 7th line, we make the backup file name into a HH:MM dd:mm:YY format.
4. ```lowercase``` and ```uppercase``` is to initialized the alphabet string in both uppercase and lowercase format. It is repeated twice to make sure that every character can be encrypted on the backup file.
5. Because we want to encrypt the syslog file to make it as the backup, I used ```cat``` to read the **/var/log/syslog** file.
6. We used ```tr``` to do the encryption to the contents of the file. And the contents that's already encrypted will be moved and saved on the **backup** file

And then we make a file called ```log_decrypt.sh``` where we will store the code that will do the decryption

```shell script
$ nano log_decrypt.sh
```
Source code for log_decrypt.sh:

```shell script
#!/bin/bash

encrypted=$(ls -t /Home/Documents/soal4/encrypt/ | head -n1)

hour=$(date "+%H")

encrypted_path="/Home/Documents/soal4/encrypt/${encrypted}"
decrypted_path="/Home/Documents/soal4/decrypt/${encrypted}"

lowercase="abcdefghijklmnopqrstuvwxyz"
uppercase="ABCDEFGHIJKLMNOPQRSTUVWXYZ"

shift_hour=$((hour % 26))

if [ -f "$encrypted_path" ] ; then
cat "${encrypted_path}" | tr "${lowercase:${shift_hour}}${uppercase:${shift_hour}}" "${lowercase}${uppercase}" > "${decrypted_path}"
fi
```
1. ```encrypted``` ```encrypted_path``` and ```decrypted_path``` is used to take the newest file on the encrypt directory. 
2. ```ls -t``` is used to show all the files in order according to the newest modification. 
3. Because we want to take the newest file, we use ```head -n1``` to take the file that is on the very top. 
4. we use ```hour=$(date "+%H")``` to record the current hour on the hour variable.
5. ```lowercase``` and ```uppercase``` is to initialized the alphabet string in both uppercase and lowercase format. It is repeated twice to prevent error when we do the character shift.
6. ```if``` is used to check if there is a new file on the encrypt directory.
7. ```tr``` is used to do the character subtitution.
8. File will be decrypted by changing every character based on the cipher shift method. The result of decryption will be stored in a new file on the decrypt directory.

To make it automatically do its job every 2 hours, we use ```cron job```:
```shell script
$ crontab -e
```
which contain:
```shell script
0 */2 * * * /bin/bash /home/Documents/soal4/log_encrypt.sh
0 */2 * * * /bin/bash /home/Documents/soal4/log_decrypt.sh
```
the first line is to do the encryption automatically, and the second line is to do the decryption automatically. 
