#!/bin/bash

echo -e "Here are the top 5 of the University in Japan based on the 2023 QS World University Rankings:\n"

awk '/Japan/{print}' '2023 QS World University Rankings.csv' | head -n 5

echo -e "\nThe university with the lowest Faculty Student Score in Japan is:\n"

grep -E Japan '2023 QS World University Rankings.csv' | sort -t, -k9n | head -n 5

echo -e "\nHere are the top 10 university in Japan that has the highest Employment Outcome Rank:\n"

awk '/Japan/' '2023 QS World University Rankings.csv' | sort -t, -k20n | grep -m 10 Japan


answer="keren"
another_answer="Keren"
echo -e "\nplease enter the keyword:"
read keyword

if [ $keyword  == $answer ]
then 
awk '/Keren/{print}' '2023 QS World University Rankings.csv'
elif [ $keyword == $another_answer ]
then
awk '/Keren/{print}' '2023 QS World University Rankings.csv'
else
echo "sorry wrong keyword"
fi

