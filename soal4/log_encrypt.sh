#!/bin/bash

hour=$(date "+%H")

date=$(date "+%H:%M %d:%m:%Y")

backup="/home/Documents/soal4/encrypt/${date}.txt"

lowercase="abcdefghijklmnopqrstuvwxyz"
uppercase="ABCDEFGHIJKLMNOPQRSTUVWXYZ"

shift_hour=$((hour % 26))

cat /var/log/syslog | tr "${lowercase}${uppercase}" "${lowercase:${shift_hour}}${uppercase:${shift_hour}}" > "${backup}"

#cron:
#0 */2 * * * /bin/bash /home/Documents/soal4/log_encrypt.sh
#0 */2 * * * /bin/bash /home/Documents/soal4/log_decrypt.sh

