#!/bin/bash

encrypted=$(ls -t /Home/Documents/soal4/encrypt/ | head -n1)

hour=$(date "+%H")

encrypted_path="/Home/Documents/soal4/encrypt/${encrypted}"
decrypted_path="/Home/Documents/soal4/decrypt/${encrypted}"

lowercase="abcdefghijklmnopqrstuvwxyz"
uppercase="ABCDEFGHIJKLMNOPQRSTUVWXYZ"

shift_hour=$((hour % 26))

if [ -f "$encrypted_path" ] ; then
cat "${encrypted_path}" | tr "${lowercase:${shift_hour}}${uppercase:${shift_hour}}" "${lowercase}${uppercase}" > "${decrypted_path}"
fi
