#!/bin/bash
image="https://upload.wikimedia.org/wikipedia/commons/thumb/e/ed/Bung_Tomo.jpg/640px-Bung_Tomo.jpg"

download(){
	if [[ ! -f counter ]] ; then
		echo 1 > counter
	fi
	
	if [[ -f counter ]] ; then
		counter=$(cat counter)
	fi
	
	if [[ ! -d "kumpulan_$counter" ]] ; then
		mkdir -p kumpulan_$counter
		counter=$((counter+1))
		echo $counter > counter
	fi
	
	count=$(cat counter)
	hour=$(date +'%H')
	hour=$((10#$hour))
	if [[ $hour -eq 0 ]] ; then
		wget "$image" -o "perjalanan_1.jpg"
	else
		for i in $(seq 1 $hour) ; do
			wget "$image" -O "perjalanan_$i.jpg"
		done
	fi
	
	mv perjalanan_*.jpg "kumpulan_$((counter-1))"
}

zipfile(){

	if [[ ! -f counter ]] ; then
		echo 1 > counter
	fi
	
	if [[ -f counter ]] ; then
		counter=$(cat counter)
	fi
	
	if [[ ! -d "kumpulan_$counter" ]] ; then
		counter=$((counter+1))
		echo $counter > counter
	fi
	count=$(cat counter)
	
	zip "devil_$((counter-1)).zip" kumpulan_* 
}



case $1 in
	-download) download ;;
	-zip) zipfile ;;
esac


#Cronjob
#0 */12 * * * cd /home/fadhlan/sisop-praktikum-modul-1-2023-bj-u07/soal2 && ./kobeni_liburan.sh -download
#0 0 * * * cd /home/fadhlan/sisop-praktikum-modul-1-2023-bj-u07/soal2 && ./kobeni_liburan.sh -zip